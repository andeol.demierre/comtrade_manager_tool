# -*- coding: utf-8 -*-
#####################################################################
# Author  : Lucas Andrey
# Date    : 28.06.2022
# Version : 1.2
# Mail    : lucas.andrey@hefr.ch
#####################################################################
# MIT License
#
# Copyright (c) 2021 Lucas Andrey
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import os
import ctypes
# import csv
# import datetime as dt
from dateutil.parser import parse
from tkinter import *
from tkinter import ttk
from tkinter.filedialog import askopenfilename
from tkinter import messagebox
# import matplotlib.pyplot as plt
from comtrade import Comtrade
from comtrade_v4 import Comtrade as Comtrade_v4 # comtrade_v4 is for problem with Sprecher comtrade files default => comtrade
from heia_signal_toolbox import *

BG = '#adcdff'
BG_BUTTON = '#3bbaff'
BG_BUTTON_G = '#c5d1e3'
BG_BUTTON_RUN = '#6df75e'
BG_BUTTON_QUIT = '#fc633d'
BG_MENU = '#e4eaed'
PAD = 5
FULL_DT_FORMAT = "%d/%m/%Y %H:%M:%S.%f"
SHORT_DT_FORMAT = "%Y%m%d_%H%M%S"
EXPORT_PATH = "Export_files/"
EXPORT_NAME = "export_sig_"
DAT_EXT = ".dat"
CFG_EXT = ".cfg"
CSV_EXT = ".csv"
CMT_SEP = ","
CSV_SEP = ";"
MAX_CMT_LIM = 65535
MIN_CMT_LIM = -65536
global_mp_selected_sig_list = list()


def compute_a(v, lim_pos, lim_neg) -> float:
	if lim_pos != lim_neg:
		max_val = np.max(v)
		min_val = np.min(v)
		ret = (max_val - min_val) / (lim_pos - lim_neg)
		if ret == 0.0:
			ret = 1.0
	else:
		ret = 1.0
	return ret


def compute_b(v, lim_pos, lim_neg) -> float:
	if lim_pos != lim_neg:
		max_val = np.max(v)
		min_val = np.min(v)
		ret = float(((min_val * lim_pos) - (max_val * lim_neg)) / (lim_pos - lim_neg))
	else:
		ret = 0.0
	return ret


class LogicCmtManager:
	def __init__(self):
		self.stop_action = FALSE
		self.MySignals = MultiSignal()
		self.sel_signals = list()
		self.next_window = int(0)
	
	def run_gui(self):
		while 0 <= self.next_window < 999:
			if self.next_window == 0:
				self.run_load_page()
			elif self.next_window == 100:
				self.run_main_page()
			elif self.next_window == 200:
				self.run_signals_information()
			elif self.next_window == 300:
				self.plot_signals()
			elif self.next_window == 400:
				self.compute_avg_freq()
			elif self.next_window == 500:
				self.resample_time()
			elif self.next_window == 501:
				self.change_time_interval()
			elif self.next_window == 502:
				self.decimate_signals()
			elif self.next_window == 503:
				self.time_offset()
			elif self.next_window == 600:
				self.linear_rescale()
			elif self.next_window == 700:
				self.change_signal_info()
			elif self.next_window == 800:
				self.export()
			elif self.next_window == 900:
				self.remove_sig()
				self.select_next_window(100)
			else:
				if messagebox.askokcancel("Quit", "Do you want to quit?"):
					self.quit_gui()
				else:
					self.select_next_window(100)
	
	def select_next_window(self, nw: int):
		if self.next_window != 999:
			self.next_window = nw
	
	def write_selected_signals(self, selected_sig_list: list):
		self.sel_signals.clear()
		for sig_num in selected_sig_list:
			self.sel_signals.append(int(sig_num))
	
	# print(self.sel_signals)
	
	def quit_gui(self):
		self.next_window = 999
	
	def run_load_page(self):
		wp = WelcomePage(self.load_csv_cfg_dat_file, self.select_next_window, self.quit_gui)
		wp.wp_launch()
		wp.wp.destroy()
		print("Number of signals loaded : " + str(self.MySignals.nb_sig()))
		del wp
	
	def run_main_page(self):
		my_list = self.MySignals.ret_name_list()
		mp = MainPage(my_list, self.write_selected_signals, self.select_next_window, self.quit_gui)
		mp.mp_launch()
		mp.mp.destroy()
		del mp
	
	def run_signals_information(self):
		print("run_signals_information")
		self.MySignals.print_sel_sig_info(self.sel_signals)
		self.select_next_window(100)
	
	def plot_signals(self):
		print("plot_signals")
		self.MySignals.plot_sel_sig(self.sel_signals)
		self.select_next_window(100)
	
	def compute_avg_freq(self):
		print("Compute_avg_freq")
		self.MySignals.compute_sel_sig_freq(self.sel_signals)
		self.select_next_window(100)
	
	def resample_time(self):
		print("resample_time")
		# basic check
		cp_check = TRUE
		if self.sel_signals:
			# compatibility check
			max_size = self.MySignals.sig[self.sel_signals[0]].size
			min_time = self.MySignals.sig[self.sel_signals[0]].get_min_time()
			max_time = self.MySignals.sig[self.sel_signals[0]].get_max_time()
			max_fs = self.MySignals.sig[self.sel_signals[0]].get_sample_freq()
			for n in self.sel_signals:
				if max_size != self.MySignals.sig[n].size:
					if max_size < self.MySignals.sig[n].size:
						max_size = self.MySignals.sig[n].size
					cp_check = FALSE
				if (min_time != self.MySignals.sig[n].get_min_time()) or (
						max_time != self.MySignals.sig[n].get_max_time()):
					if min_time > self.MySignals.sig[n].get_min_time():
						min_time = self.MySignals.sig[n].get_min_time()
					if max_time < self.MySignals.sig[n].get_max_time():
						max_time = self.MySignals.sig[n].get_max_time()
					cp_check = FALSE
				if max_fs != self.MySignals.sig[n].get_sample_freq():
					if max_fs < self.MySignals.sig[n].get_sample_freq():
						max_fs = self.MySignals.sig[n].get_sample_freq()
					cp_check = FALSE
		else:
			self.select_next_window(100)
			return 0
		est_size = (max_time - min_time) * max_fs
		print("*---------------------------------------------------------*")
		print("First analysis results :")
		print("Fs         :" + str(max_fs))
		print("Estimated size :" + str(est_size))
		print("Start time :" + str(max_time))
		print("End time   :" + str(min_time))
		print("*---------------------------------------------------------*")
		# Corrections for compatibility
		print(max_size)
		if cp_check == FALSE:
			if self.ok_modif():
				for n in self.sel_signals:
					if (min_time != self.MySignals.sig[n].get_min_time()) or (
							max_time != self.MySignals.sig[n].get_max_time()):
						print("correction of " + str(n))
						# if self.MySignals.sig[n].unit.capitalize() == "HZ":
						# 	self.MySignals.sig[n].change_time_interval(min_time, max_time, 50)
						self.MySignals.sig[n].change_time_interval(min_time, max_time, 0)
				# self.MySignals.sig[n].plot()
				for n in self.sel_signals:
					if max_size < self.MySignals.sig[n].size:
						max_size = self.MySignals.sig[n].size
						print(max_size)
				for n in self.sel_signals:
					if max_size != self.MySignals.sig[n].size:
						print("correction of " + str(n))
						self.MySignals.sig[n].change_size_0_order(max_size)
		else:
			self.select_next_window(100)
			return 0
		print("*---------------------------------------------------------*")
		print("Resample to common time vector results :")
		print("Fs         :" + str(max_fs))
		print("Final size :" + str(max_size))
		print("Start time :" + str(max_time))
		print("End time   :" + str(min_time))
		print("*---------------------------------------------------------*")
		self.select_next_window(100)
	
	def decimate_signals(self):
		print("decimate_signals")
		dec_val = int(2)
		ds = DecimateSignal(dec_val)
		dec_val = ds.ds_launch()
		ds.ds.destroy()
		del ds
		print(dec_val)
		if dec_val >= 2:
			for i in self.sel_signals:
				self.MySignals.sig[i].decimate(dec_val, 0)
		else:
			print("Nothing was done...")
		self.select_next_window(100)
	
	def change_time_interval(self):
		print("change_time_interval")
		self.stop_chain(FALSE)
		for i in self.sel_signals:
			if self.stop_action == 0:
				cti = ChangeTimeInterval(self.MySignals.sig[i], self.stop_chain,
				                         self.MySignals.sig[i].change_time_interval)
				cti.cti_launch()
				cti.cti.destroy()
				del cti
		self.select_next_window(100)
	
	def time_offset(self):
		print("time_offset")
		self.stop_chain(FALSE)
		for i in self.sel_signals:
			if self.stop_action == 0:
				to = TimeOffset(self.MySignals.sig[i], self.stop_chain,
				                self.MySignals.sig[i].offset_time)
				to.to_launch()
				to.to.destroy()
				del to
		self.select_next_window(100)
	
	def linear_rescale(self):
		print("linear_rescale")
		self.stop_chain(FALSE)
		for i in self.sel_signals:
			if self.stop_action == 0:
				lr = LinearRescale(self.MySignals.sig[i], self.stop_chain)
				lr.lr_launch()
				lr.lr.destroy()
				del lr
		self.select_next_window(100)
	
	def stop_chain(self, val: bool):
		self.stop_action = val
	
	def change_signal_info(self):
		print("change_info")
		self.stop_chain(FALSE)
		for i in self.sel_signals:
			if self.stop_action == 0:
				msi = ModifySigInformation(self.MySignals.sig[i], self.stop_chain, self.MySignals.sig[i].change_name,
				                           self.MySignals.sig[i].load_unit, self.MySignals.sig[i].load_start_dt,
				                           self.MySignals.sig[i].load_trig_dt)
				msi.msi_launch()
				msi.msi.destroy()
				del msi
		self.select_next_window(100)
	
	def ok_modif(self) -> bool:
		root = Tk()
		temp = messagebox.askokcancel("Modify signals",
		                              "Signals are not compatible. Do you want to resample these signals?")
		if temp:
			root.destroy()
			return TRUE
		else:
			root.destroy()
			return FALSE
	
	def export(self):
		# basic check
		cp_check = TRUE
		if self.sel_signals:
			# compatibility check
			max_size = self.MySignals.sig[self.sel_signals[0]].size
			min_time = self.MySignals.sig[self.sel_signals[0]].get_min_time()
			max_time = self.MySignals.sig[self.sel_signals[0]].get_max_time()
			max_fs = self.MySignals.sig[self.sel_signals[0]].get_sample_freq()
			for n in self.sel_signals:
				if max_size != self.MySignals.sig[n].size:
					if max_size < self.MySignals.sig[n].size:
						max_size = self.MySignals.sig[n].size
					cp_check = FALSE
				if (min_time != self.MySignals.sig[n].get_min_time()) or (
						max_time != self.MySignals.sig[n].get_max_time()):
					if min_time > self.MySignals.sig[n].get_min_time():
						min_time = self.MySignals.sig[n].get_min_time()
					if max_time < self.MySignals.sig[n].get_max_time():
						max_time = self.MySignals.sig[n].get_max_time()
					cp_check = FALSE
				if max_fs != self.MySignals.sig[n].get_sample_freq():
					if max_fs < self.MySignals.sig[n].get_sample_freq():
						max_fs = self.MySignals.sig[n].get_sample_freq()
					cp_check = FALSE
		else:
			self.select_next_window(100)
			return 0
		
		# Corrections for compatibility
		print(max_size)
		if cp_check == FALSE:
			if self.ok_modif():
				for n in self.sel_signals:
					if (min_time != self.MySignals.sig[n].get_min_time()) or (
							max_time != self.MySignals.sig[n].get_max_time()):
						print("correction of " + str(n))
						# if self.MySignals.sig[n].unit.capitalize() == "HZ":
						# 	self.MySignals.sig[n].change_time_interval(min_time, max_time, 50)
						self.MySignals.sig[n].change_time_interval(min_time, max_time, 0)
				# self.MySignals.sig[n].plot()
				for n in self.sel_signals:
					if max_size < self.MySignals.sig[n].size:
						max_size = self.MySignals.sig[n].size
						print(max_size)
				for n in self.sel_signals:
					if max_size != self.MySignals.sig[n].size:
						print("correction of " + str(n))
						self.MySignals.sig[n].change_size_0_order(max_size)
			else:
				self.select_next_window(100)
				return 0
		
		print("export")
		total_samples = self.MySignals.sig[self.sel_signals[0]].size
		calculate_sample_rates = round(
			(total_samples - 1) / (self.MySignals.sig[self.sel_signals[0]].time[total_samples - 1]
			                       - self.MySignals.sig[self.sel_signals[0]].time[0]), 5)
		analog_count = len(self.sel_signals)
		init_dt = dt.datetime.now()
		init_dt_str = init_dt.strftime(SHORT_DT_FORMAT)
		# Variables
		st_name = "Comtrade Manager V01.00"
		dev_id = init_dt_str
		comtrade_year = "1999"
		
		line_freq = 50.0
		
		# CFG FILE
		a = []
		b = []
		cfg = open(EXPORT_PATH + EXPORT_NAME + init_dt_str + CFG_EXT, "w+")
		cfg.write(st_name + CMT_SEP + dev_id + CMT_SEP + comtrade_year + "\n")
		cfg.write(str(analog_count) + CMT_SEP + str(analog_count) + "A" + CMT_SEP + "00D" + "\n")
		for i in range(analog_count):
			a.append(compute_a(self.MySignals.sig[self.sel_signals[i]].data, MAX_CMT_LIM, MIN_CMT_LIM))
			b.append(compute_b(self.MySignals.sig[self.sel_signals[i]].data, MAX_CMT_LIM, MIN_CMT_LIM))
			cfg.write(str(i + 1) + CMT_SEP + self.MySignals.sig[self.sel_signals[i]].name + CMT_SEP + CMT_SEP + CMT_SEP
			          + self.MySignals.sig[self.sel_signals[i]].unit + CMT_SEP)
			cfg.write(str(format(a[i], '.6e')) + CMT_SEP + str(format(b[i], '.6e')) + CMT_SEP + "0" + CMT_SEP)
			cfg.write(str(MIN_CMT_LIM) + CMT_SEP + str(MAX_CMT_LIM) + CMT_SEP + "1" + CMT_SEP + "1" + CMT_SEP + "P\n")
		cfg.write(str(round(line_freq)) + "\n")
		cfg.write("1\n")
		cfg.write(str(calculate_sample_rates) + CMT_SEP + str(total_samples) + "\n")
		str_start_time = self.MySignals.sig[self.sel_signals[0]].start_dt.strftime("%d/%m/%Y,%H:%M:%S.%f") + "\n"
		cfg.write(str_start_time)
		str_trig_time = self.MySignals.sig[self.sel_signals[0]].trig_dt.strftime("%d/%m/%Y,%H:%M:%S.%f") + "\n"
		cfg.write(str_trig_time)
		cfg.write("ASCII\n")
		cfg.write(str(round(1.0, 1)))
		cfg.close()
		
		# DAT FILE
		# print(a)
		dat = open(EXPORT_PATH + EXPORT_NAME + init_dt_str + DAT_EXT, "w+")
		for i in range(total_samples):
			dat.write(str(format(i + 1, '0>10d')) + CMT_SEP)
			dat.write(str(format(int(self.MySignals.sig[self.sel_signals[0]].time[i] * 1000000), '0>9d')) + CMT_SEP)
			for j in range(analog_count):
				signal = self.MySignals.sig[self.sel_signals[j]].data
				dat_val = str(round((signal[i] - b[j]) / a[j]))
				dat.write(dat_val)
				if j < analog_count - 1:
					dat.write(CMT_SEP)
			dat.write("\n")
		dat.close()
		
		# COPY CSV
		# 1st line
		csv = open(EXPORT_PATH + EXPORT_NAME + init_dt_str + CSV_EXT, "w+")
		csv.write("n" + CSV_SEP)
		csv.write("time" + CSV_SEP)
		for j in range(analog_count):
			csv.write(self.MySignals.sig[self.sel_signals[j]].name)
			if j < analog_count - 1:
				csv.write(CSV_SEP)
		csv.write("\n")
		# 2nd line
		csv.write("-" + CSV_SEP)
		csv.write("us" + CSV_SEP)
		for j in range(analog_count):
			csv.write(self.MySignals.sig[self.sel_signals[j]].unit)
			if j < analog_count - 1:
				csv.write(CSV_SEP)
		csv.write("\n")
		# 3rd line
		csv.write("-" + CSV_SEP)
		csv.write("-" + CSV_SEP)
		for j in range(analog_count):
			temp = self.MySignals.sig[self.sel_signals[j]].start_dt.strftime(FULL_DT_FORMAT)
			csv.write(temp)
			if j < analog_count - 1:
				csv.write(CSV_SEP)
		csv.write("\n")
		# 4th line
		csv.write("-" + CSV_SEP)
		csv.write("-" + CSV_SEP)
		for j in range(analog_count):
			temp = self.MySignals.sig[self.sel_signals[j]].trig_dt.strftime(FULL_DT_FORMAT)
			csv.write(temp)
			if j < analog_count - 1:
				csv.write(CSV_SEP)
		csv.write("\n")
		# Last lines
		for i in range(total_samples):
			csv.write(str(format(i + 1, '0>10d')) + CSV_SEP)
			csv.write(str(format(int(self.MySignals.sig[self.sel_signals[0]].time[i] * 1000000), '0>9d')) + CSV_SEP)
			for j in range(analog_count):
				signal = self.MySignals.sig[self.sel_signals[j]].data
				dat_val = str(format(signal[i], '.6e'))
				csv.write(dat_val)
				if j < analog_count - 1:
					csv.write(CSV_SEP)
			csv.write("\n")
		csv.close()
		self.select_next_window(100)
	
	def remove_sig(self):
		global global_mp_selected_sig_list
		print("removed")
		for i in range(len(self.sel_signals)):
			self.MySignals.pop_sig(self.sel_signals[i] - i)
		global_mp_selected_sig_list = list()
		self.select_next_window(100)
	
	def load_csv_cfg_dat_file(self):
		root = Tk()
		root.withdraw()
		name = askopenfilename()
		if name:
			file_ext = name[-3:].upper()
			basename = name[:-3]
			if (file_ext == "CFG") or (file_ext == "DAT"):
				print("You have selected this file: " + basename + file_ext)
				if os.path.isfile(basename + "CFG") and os.path.isfile(basename + "DAT"):
					try:
						rec = Comtrade()
						rec.load(basename + "CFG", basename + "DAT")
					except ValueError:
						print("DEBUG MODE ON : ValueError")
						rec = Comtrade_v4()
						rec.load(basename + "CFG", basename + "DAT")
					units = [u.uu for u in rec.cfg.analog_channels]
					total_samples = int(rec.total_samples)
					print(
						"Station: " + rec.station_name + " / Device ID: " + rec.rec_dev_id + " / Revision year: " + str(
							rec.rev_year))
					# --------------------------------------------------------------------------------------------------
					# Load Comtrade in Multi-signal
					# --------------------------------------------------------------------------------------------------
					for i in range(rec.analog_count):
						# print(".")
						new_sig = Signal(str(rec.analog_channel_ids[i]), str(units[i]), total_samples)
						time_val = []
						data = []
						cmt_data = rec.analog[i]
						for j in range(total_samples):
							time_val.append(float(rec.time[j]))
							data.append(float(cmt_data[j]))
						new_sig.load_start_dt(rec.cfg.start_timestamp)
						new_sig.load_trig_dt(rec.cfg.trigger_timestamp)
						new_sig.load_time(time_val)
						new_sig.load_data(data)
						self.MySignals.add_new_sig(new_sig)
					# print(new_sig.name)
					for i in range(rec.status_count):
						# print(".")
						new_sig = Signal(str(rec.status_channel_ids[i]), " - ", total_samples)
						time_val = []
						data = []
						cmt_data = rec.status[i]
						for j in range(total_samples):
							time_val.append(float(rec.time[j]))
							data.append(float(cmt_data[j]))
						new_sig.load_start_dt(rec.cfg.start_timestamp)
						new_sig.load_trig_dt(rec.cfg.trigger_timestamp)
						new_sig.load_time(time_val)
						new_sig.load_data(data)
						self.MySignals.add_new_sig(new_sig)
					# print(new_sig.name)
					del rec
				else:
					ctypes.windll.user32.MessageBoxW(0, "CFG and DAT files must have the same name...",
					                                 "The CFG or DAT file could not be found...", 0)
			elif file_ext == "CSV":
				print("You have selected this file: " + basename + file_ext)
				if os.path.isfile(basename + "CSV"):
					csv_file = open(basename + "CSV", 'r')
					print(csv_file.readline())
					csv_file.close()
					# ------------------------------------------------------------------------------------------------------
					# Load CSV in Multi-signal
					# ------------------------------------------------------------------------------------------------------
					ctypes.windll.user32.MessageBoxW(0, "The CSV file import functionality is not yet implemented ...",
					                                 "The CSV file could not be loaded...", 0)
				else:
					ctypes.windll.user32.MessageBoxW(0, "There is a error with the CSV file path...",
					                                 "The CSV file could not be found...", 0)
			else:
				ctypes.windll.user32.MessageBoxW(0, "You can only select CSV, CFG or DAT files...",
				                                 "Wrong file extension...", 0)


class MainPage:
	def __init__(self, sig_list: list, sel_signals_fct, selection_fct, quit_fct):
		# Main Page
		global global_mp_selected_sig_list
		self.action = str()
		self.selected_sig = list()
		self.sig_list = sig_list
		self.sel_signal_fct = sel_signals_fct
		self.select_fct = selection_fct
		self.quit_fct = quit_fct
		self.mp = Tk()
		self.mp.title("Comtrade Manager Main Page")
		self.mp.geometry("960x480")
		self.mp.minsize(720, 320)
		self.mp.iconbitmap("heia_logo3.ico")
		self.mp.config(background=BG)
		# init comp
		self.mp_menu_bar = Menu(self.mp, bg=BG_MENU)
		self.mp_canvas = Canvas(self.mp, width=720, height=100, bg=BG, bd=0, highlightthickness=0)
		self.mp_frameL = Frame(self.mp, bg=BG)
		self.mp_frameR = Frame(self.mp, bg=BG)
		self.mp_frame_scroll_box = Frame(self.mp_frameL, bg='white')
		self.mp_scrollbar = Scrollbar(self.mp_frame_scroll_box)
		self.mp_scrollbar.pack(side=RIGHT, fill=Y)
		self.my_list = Listbox(self.mp_frame_scroll_box, selectmode="multiple", exportselection=False,
								yscrollcommand=self.mp_scrollbar.set)
		self.mp_actions = ["Display signals information", "Plot signals", "Compute average frequency",
							"Decimate selected signals", "Resample to common time vector", "Change time interval",
							"Time offset", "Linear rescale", "Change signals info", "Export selected to Comtrade or CSV",
							"Remove selected signals"]
		self.mp_list_combo = ttk.Combobox(self.mp_frameR, font=("Courrier", 20), values=self.mp_actions)
		self.button_continue = Button(self.mp_frameR, text="Run", font=("Courrier", 20), bg=BG_BUTTON_RUN,
										command=self.mp_action)
		self.button_select_all = Button(self.mp_frameR, text="Select all signals", font=("Courrier", 20), bg=BG_BUTTON_G,
										command=self.mp_select_all_sig)
		self.button_quit = Button(self.mp_frameR, text="Quit", font=("Courrier", 20), bg=BG_BUTTON_QUIT,
										command=self.mp_quit)
		
		# Create image
		self.logo = PhotoImage(file="logo_heia-fr.png").zoom(10).subsample(35)
	
	def mp_launch(self):
		# Creation of components
		
		# Menu
		file_menu = Menu(self.mp_menu_bar, tearoff=0, bg=BG_MENU)
		file_menu.add_command(label="Load New File", command=self.mp_load_new_file)
		# file_menu.add_command(label="Save All", command=self.mp_continue)
		# file_menu.add_command(label="Open Main Page", command=self.mp_continue)
		self.mp_menu_bar.add_cascade(label="Options", menu=file_menu)
		self.mp_menu_bar.add_command(label="Quit GUI", command=self.mp_quit)
		
		# Image HEIA-FR
		width = 720
		height = 100
		self.mp_canvas.create_image(width / 2, height / 2, image=self.logo)
		
		# Label de selection des signaux
		label_title = Label(self.mp_frameL, text="2) Choose your signals :", font=("Courrier", 22), bg=BG, fg='black')
		label_title.pack(side=TOP)
		# Liste de signaux
		for line in self.sig_list:
			self.my_list.insert(END, str(line))
		self.my_list.pack(fill=BOTH, expand=YES)
		# Scroll bar
		self.mp_scrollbar.config(command=self.my_list.yview)
		
		# Label de selection des actions
		label_title = Label(self.mp_frameR, text="1) Choose your action :", font=("Courrier", 22), bg=BG, fg='black')
		label_title.pack(side=TOP)
		self.mp_list_combo.current(0)
		
		# packing and config
		self.mp.config(menu=self.mp_menu_bar)
		self.mp_canvas.pack(pady=PAD, padx=PAD)
		self.mp_frameL.pack(side=RIGHT, expand=YES, fill="both", pady=PAD, padx=PAD)
		self.mp_frame_scroll_box.pack(fill="both", expand=YES, pady=PAD, padx=PAD)
		self.mp_frameR.pack(side=LEFT, expand=YES, fill="both", pady=PAD, padx=PAD)
		self.mp_list_combo.pack(fill=X, pady=PAD, padx=PAD)
		self.button_select_all.pack(fill=X, pady=PAD, padx=PAD)
		self.button_continue.pack(fill=X, pady=PAD, padx=PAD)
		self.button_quit.pack(fill=X, pady=PAD, padx=PAD)
		
		# Protocol
		self.mp.protocol("WM_DELETE_WINDOW", self.mp_quit)
		
		# Old selection
		for i in range(len(global_mp_selected_sig_list)):
			self.my_list.select_set(global_mp_selected_sig_list[i],global_mp_selected_sig_list[i])
		
		# Run Welcome page
		self.mp.mainloop()
	
	def mp_quit(self):
		if messagebox.askokcancel("Quit", "Do you want to quit?"):
			self.quit_fct()
		else:
			self.select_fct(100)
		self.mp.quit()
	
	def mp_continue(self):
		self.select_fct(100)
		self.mp.quit()
	
	def mp_select_all_sig(self):
		if len(self.sig_list) == len(self.my_list.curselection()):
			self.my_list.select_clear(0, END)
		else:
			self.my_list.select_set(0, END)
	
	def mp_action(self):
		global global_mp_selected_sig_list
		self.action = str(self.mp_list_combo.get())
		self.selected_sig = self.my_list.curselection()
		global_mp_selected_sig_list = self.selected_sig
		self.sel_signal_fct(self.selected_sig)
		print("Action = " + self.action)
		print("Signals = " + str(self.selected_sig))
		if self.action == "Display signals information":
			print(200)
			self.select_fct(200)
		elif self.action == "Plot signals":
			print(300)
			self.select_fct(300)
		elif self.action == "Compute average frequency":
			print(400)
			self.select_fct(400)
		elif self.action == "Resample to common time vector":
			print(500)
			self.select_fct(500)
		elif self.action == "Change time interval":
			print(501)
			self.select_fct(501)
		elif self.action == "Decimate selected signals":
			print(502)
			self.select_fct(502)
		elif self.action == "Time offset":
			print(503)
			self.select_fct(503)
		elif self.action == "Linear rescale":
			print(600)
			self.select_fct(600)
		elif self.action == "Change signals info":
			print(700)
			self.select_fct(700)
		elif self.action == "Export selected to Comtrade or CSV":
			print(800)
			self.select_fct(800)
		elif self.action == "Remove selected signals":
			print(900)
			self.select_fct(900)
		else:
			self.select_fct(100)
		self.mp.quit()
	
	def mp_load_new_file(self):
		self.select_fct(0)
		self.mp.quit()


class WelcomePage:
	def __init__(self, load_fct, selection_fct, quit_fct):
		# Welcome Page
		self.load_fct = load_fct
		self.select_fct = selection_fct
		self.quit_fct = quit_fct
		self.wp = Tk()
		self.wp.title("Comtrade Manager")
		self.wp.geometry("960x480")
		self.wp.minsize(720, 320)
		self.wp.iconbitmap("heia_logo3.ico")
		self.wp.config(background=BG)
		# init comp
		self.wp_frame = Frame(self.wp, bg=BG)
		self.wp_menu_bar = Menu(self.wp, bg=BG_MENU)
		self.wp_canvas = Canvas(self.wp, width=720, height=100, bg=BG, bd=0, highlightthickness=0)
		# Create image
		self.logo = PhotoImage(file="logo_heia-fr.png").zoom(10).subsample(35)
	
	def wp_launch(self):
		width = 720
		height = 100
		self.wp_canvas.create_image(width / 2, height / 2, image=self.logo)
		
		# creation des composants
		# Menu
		file_menu = Menu(self.wp_menu_bar, tearoff=0, bg=BG_MENU)
		file_menu.add_command(label="Open Main Page", command=self.wp_continue)
		file_menu.add_command(label="Load New File", command=self.load_fct)
		# file_menu.add_command(label="Save All ", command=self.wp_continue)
		self.wp_menu_bar.add_cascade(label="Options", menu=file_menu)
		self.wp_menu_bar.add_command(label="Quit GUI", command=self.wp_quit)
		
		# Label Title
		label_title = Label(self.wp_frame, text="Please select your files", font=("Courrier", 28), bg=BG, fg='black')
		label_title.pack(pady=PAD, padx=PAD)
		label_subtitle = Label(self.wp_frame, text="Comtrade file manager version 1.2", font=("Courrier", 22), bg=BG,
		                       fg='black')
		label_subtitle.pack(pady=PAD, padx=PAD)
		
		# Buttons
		wp_button = Button(self.wp_frame, text="Choose Comtrade or CSV file", font=("Courrier", 22), bg=BG_BUTTON,
		                   command=self.load_fct)
		wp_button.pack(fill=X, pady=PAD, padx=PAD)
		wp_button = Button(self.wp_frame, text="Continue", font=("Courrier", 22), bg=BG_BUTTON,
		                   command=self.wp_continue)
		wp_button.pack(fill=X, pady=PAD, padx=PAD)
		
		# packing and config
		self.wp.config(menu=self.wp_menu_bar)
		self.wp_canvas.pack(expand=YES, pady=PAD, padx=PAD)
		self.wp_frame.pack(expand=YES, pady=PAD, padx=PAD)
		
		# Protocol
		self.wp.protocol("WM_DELETE_WINDOW", self.wp_quit)
		
		# Run Welcome page
		self.wp.mainloop()
	
	def wp_quit(self):
		if messagebox.askokcancel("Quit", "Do you want to quit?"):
			self.quit_fct()
		else:
			self.select_fct(0)
		self.wp.quit()
	
	def wp_continue(self):
		self.select_fct(100)
		self.wp.quit()


class DisplaySigInformation:
	def __init__(self, signals: list):
		self.dsi = Tk()
		self.dsi.title("Comtrade Manager Signal Info")
		self.dsi.geometry("960x480")
		self.dsi.minsize(720, 320)
		self.dsi.iconbitmap("heia_logo3.ico")
		self.dsi.config(background=BG)
		# init comp
		self.dsi_title_frame = Frame(self.dsi, bg=BG)
		self.dsi_info_frame = Frame(self.dsi, bg=BG)
		self.dsi_button_frame = Frame(self.dsi, bg=BG)
	
	def dsi_launch(self):
		# Title
		label_title = Label(self.dsi_title_frame, text="Please select your files", font=("Courrier", 28),
		                    bg=BG, fg='black')
		label_title.pack(pady=PAD, padx=PAD)


class ModifySigInformation:
	def __init__(self, signal: Signal, stop_fct, chg_name, chg_unit, chg_start, chg_trig):
		self.stop_fct = stop_fct
		self.chg_name_fct = chg_name
		self.chg_unit_fct = chg_unit
		self.chg_start_fct = chg_start
		self.chg_trig_fct = chg_trig
		self.sig_info = SignalInfo(signal.name, signal.unit, signal.start_dt, signal.trig_dt)
		self.sig_max_time_s = int(signal.get_max_time())
		self.msi = Tk()
		self.msi.title("Comtrade Manager Signal Info")
		self.msi.geometry("960x480")
		self.msi.minsize(720, 320)
		self.msi.iconbitmap("heia_logo3.ico")
		self.msi.config(background=BG)
		# init comp
		self.msi_title_frame = Frame(self.msi, bg=BG)
		self.msi_info_frame_L = Frame(self.msi, bg=BG)
		self.msi_info_frame_R = Frame(self.msi, bg=BG)
		self.msi_button_frame = Frame(self.msi, bg=BG)
		# Text Zone
		self.name_entry = Entry(self.msi_info_frame_R, fg="black", bg="#d4d4d4", font=("Courrier", 22))
		self.unit_entry = Entry(self.msi_info_frame_R, fg="black", bg="#d4d4d4", font=("Courrier", 22))
		self.start_entry = Entry(self.msi_info_frame_R, fg="black", bg="#d4d4d4", font=("Courrier", 22))
		self.trig_entry = Entry(self.msi_info_frame_R, fg="black", bg="#d4d4d4", font=("Courrier", 22))
	
	def msi_launch(self):
		# Title
		label_title = Label(self.msi_title_frame, text="Modify signal information", font=("Courrier", 25),
		                    bg=BG, fg='black')
		label_title.pack(pady=PAD, padx=PAD)
		
		# Text Zone
		name_label = Label(self.msi_info_frame_L, text="Name  :", bg=BG, font=("Courrier", 22))
		unit_label = Label(self.msi_info_frame_L, text="Unit  :", bg=BG, font=("Courrier", 22))
		start_label = Label(self.msi_info_frame_L, text="Start :", bg=BG, font=("Courrier", 22))
		trig_label = Label(self.msi_info_frame_L, text="Trig  :", bg=BG, font=("Courrier", 22))
		name_label.pack(fill=X, pady=PAD, padx=PAD, anchor="nw")
		unit_label.pack(fill=X, pady=PAD, padx=PAD, anchor="nw")
		start_label.pack(fill=X, pady=PAD, padx=PAD, anchor="nw")
		trig_label.pack(fill=X, pady=PAD, padx=PAD, anchor="nw")
		
		# Text Zone
		self.name_entry.insert(0, self.sig_info.name)
		self.unit_entry.insert(0, self.sig_info.unit)
		self.start_entry.insert(0, self.sig_info.start_dt.strftime(FULL_DT_FORMAT))  # FULL_DT_FORMAT
		self.trig_entry.insert(0, self.sig_info.trig_dt.strftime(FULL_DT_FORMAT))
		self.name_entry.pack(fill=X, pady=PAD * 1.1, padx=PAD)
		self.unit_entry.pack(fill=X, pady=PAD * 1.1, padx=PAD)
		self.start_entry.pack(fill=X, pady=PAD * 1.1, padx=PAD)
		self.trig_entry.pack(fill=X, pady=PAD * 1.1, padx=PAD)
		
		# Buttons
		msi_button = Button(self.msi_button_frame, text="Exit", font=("Courrier", 22), bg=BG_BUTTON,
		                    command=self.msi_exit_fct)
		msi_button.pack(fill=X, pady=PAD, padx=PAD)
		msi_button = Button(self.msi_button_frame, text="Continue", font=("Courrier", 22), bg=BG_BUTTON,
		                    command=self.msi_continue)
		msi_button.pack(fill=X, pady=PAD, padx=PAD)
		
		# Protocol
		self.msi.protocol("WM_DELETE_WINDOW", self.msi_continue)
		
		# Pack
		self.msi_title_frame.pack(side=TOP, fill=X, pady=PAD, padx=PAD)
		self.msi_button_frame.pack(side=BOTTOM, fill=X, pady=PAD, padx=PAD)
		self.msi_info_frame_L.pack(side=LEFT, fill=BOTH, pady=PAD, padx=PAD)
		self.msi_info_frame_R.pack(side=RIGHT, expand=YES, fill=BOTH, pady=PAD, padx=PAD)
		
		# Run Welcome page
		self.msi.mainloop()
	
	def msi_exit_fct(self):
		self.stop_fct(TRUE)
		self.msi.quit()
	
	def msi_continue(self):
		name = self.name_entry.get()
		unit = self.unit_entry.get()
		start = self.start_entry.get()
		trig = self.trig_entry.get()
		if name and unit and start and trig:
			# start_dt = dt.datetime.strptime(start, FULL_DT_FORMAT)
			# trig_dt = dt.datetime.strptime(trig, FULL_DT_FORMAT)
			start_dt = parse(start)
			trig_dt = parse(trig)
			delta_td = trig_dt - start_dt
			delta_min_td = dt.timedelta(seconds=0, microseconds=0)
			delta_max_td = dt.timedelta(seconds=int(round(self.sig_max_time_s, 0)),
			                            microseconds=(self.sig_max_time_s - round(self.sig_max_time_s, 0)) * 1000000)
			print(delta_max_td)
			if delta_min_td <= delta_td <= delta_max_td:
				self.chg_name_fct(name)
				self.chg_unit_fct(unit)
				self.chg_start_fct(start_dt)
				self.chg_trig_fct(trig_dt)
				print(name)
				print(unit)
				print(start_dt.strftime(FULL_DT_FORMAT))
				print(trig_dt.strftime(FULL_DT_FORMAT))
			else:
				ctypes.windll.user32.MessageBoxW(0,
				                                 "the trigger must be greater than the start and not exceed the total duration of the signal...",
				                                 "Wrong Timestamp informations...", 0)
		else:
			ctypes.windll.user32.MessageBoxW(0, "information must not be empty...", "Empty informations...", 0)
		self.msi.quit()


# class ResampleTimeVector:
# 	def __init__(self):
# 		self.rtv = Tk()
# 		self.rtv.title("Comtrade Manager Time Vector")
# 		self.rtv.geometry("960x480")
# 		self.rtv.minsize(720, 320)
# 		self.rtv.iconbitmap("heia_logo3.ico")
# 		self.rtv.config(background=BG)


class LinearRescale:
	def __init__(self, signal: Signal, stop_fct):
		self.stop_fct = stop_fct
		self.sig_to_change = signal
		self.sig_max_val = int(signal.get_max_data())
		self.sig_min_val = int(signal.get_min_data())
		self.sig_label = signal.name + "_(" + signal.unit + ")"
		self.lr = Tk()
		self.lr.title("Linear rescale of " + self.sig_label)
		self.lr.geometry("960x480")
		self.lr.minsize(720, 320)
		self.lr.iconbitmap("heia_logo3.ico")
		self.lr.config(background=BG)
		# init comp
		self.lr_title_frame = Frame(self.lr, bg=BG)
		self.lr_function_frame = Frame(self.lr, bg=BG)
		self.lr_button_frame = Frame(self.lr, bg=BG)
		# Text Zone
		self.label1 = Label(self.lr_function_frame, text="New_value = ", bg=BG, font=("Courrier", 22))
		self.a_entry = Entry(self.lr_function_frame, fg="black", bg="#d4d4d4", font=("Courrier", 22))
		self.label2 = Label(self.lr_function_frame, text=" * Old_value + ", bg=BG, font=("Courrier", 22))
		self.b_entry = Entry(self.lr_function_frame, fg="black", bg="#d4d4d4", font=("Courrier", 22))
	
	def lr_launch(self):
		# Title
		label_title = Label(self.lr_title_frame, text="Linear rescale of :", font=("Courrier", 25),
		                    bg=BG, fg='black')
		label_title.pack(pady=PAD, padx=PAD)
		label_title = Label(self.lr_title_frame, text=self.sig_label, font=("Courrier", 25),
		                    bg=BG, fg='black')
		label_title.pack(pady=PAD, padx=PAD)
		
		# Text Zone
		self.a_entry.insert(0, str(1))
		self.b_entry.insert(0, str(0))
		self.label1.pack(side=LEFT, pady=PAD)
		self.a_entry.pack(side=LEFT, pady=PAD * 1.1)
		self.label2.pack(side=LEFT, pady=PAD)
		self.b_entry.pack(side=LEFT, pady=PAD * 1.1)
		
		# Buttons
		lr_button = Button(self.lr_button_frame, text="Exit", font=("Courrier", 22), bg=BG_BUTTON,
		                   command=self.lr_exit_fct)
		lr_button.pack(fill=X, pady=PAD, padx=PAD)
		lr_button = Button(self.lr_button_frame, text="Continue", font=("Courrier", 22), bg=BG_BUTTON,
		                   command=self.lr_continue)
		lr_button.pack(fill=X, pady=PAD, padx=PAD)
		
		# Protocol
		self.lr.protocol("WM_DELETE_WINDOW", self.lr_continue)
		
		# Pack
		self.lr_title_frame.pack(side=TOP, fill=X, pady=PAD, padx=PAD)
		self.lr_button_frame.pack(side=BOTTOM, fill=X, pady=PAD, padx=PAD)
		self.lr_function_frame.pack(side=LEFT, expand=NO, fill=Y, pady=PAD, padx=PAD)
		
		# Run Welcome page
		self.lr.mainloop()
	
	def lr_exit_fct(self):
		self.stop_fct(TRUE)
		self.lr.quit()
	
	def lr_continue(self):
		a = self.a_entry.get()
		b = self.b_entry.get()
		a_f = float(1)
		b_f = float(0)
		if a and b:
			try:
				a_f = float(a)
				b_f = float(b)
				self.sig_to_change.linear_trans_data(a_f, b_f)
				self.lr.quit()
			except ValueError:
				ctypes.windll.user32.MessageBoxW(0, "information must not be empty...", "Empty informations...", 0)
		# self.sig_to_change.linear_trans_data(a_f, b_f)
		else:
			ctypes.windll.user32.MessageBoxW(0, "Input fields do not contain values convertible to float...",
			                                 "Data type error...", 0)


# self.lr.quit()


# class ExportPage:
# 	def __init__(self):
# 		self.ep = Tk()
# 		self.ep.title("Comtrade Manager Export")
# 		self.ep.geometry("960x480")
# 		self.ep.minsize(720, 320)
# 		self.ep.iconbitmap("heia_logo3.ico")
# 		self.ep.config(background=BG)

class ChangeTimeInterval:
	def __init__(self, signal: Signal, stop_fct, chg_time_int):
		self.stop_fct = stop_fct
		self.chg_time_int_fct = chg_time_int
		self.sig_name = signal.name
		self.sig_max_time_s = float(signal.get_max_time())
		self.sig_min_time_s = float(signal.get_min_time())
		self.cti = Tk()
		self.cti.title("Comtrade Manager Change Time Interval")
		self.cti.geometry("960x480")
		self.cti.minsize(720, 320)
		self.cti.iconbitmap("heia_logo3.ico")
		self.cti.config(background=BG)
		# init comp
		self.cti_title_frame = Frame(self.cti, bg=BG)
		self.cti_info_frame_L = Frame(self.cti, bg=BG)
		self.cti_info_frame_R = Frame(self.cti, bg=BG)
		self.cti_button_frame = Frame(self.cti, bg=BG)
		# Text Zone
		self.start_time_entry = Entry(self.cti_info_frame_R, fg="black", bg="#d4d4d4", font=("Courrier", 22))
		self.stop_time_entry = Entry(self.cti_info_frame_R, fg="black", bg="#d4d4d4", font=("Courrier", 22))
		self.default_entry = Entry(self.cti_info_frame_R, fg="black", bg="#d4d4d4", font=("Courrier", 22))
	
	def cti_launch(self):
		# Title
		label_title = Label(self.cti_title_frame, text=("Change time interval of : " + self.sig_name),
		                    font=("Courrier", 25), bg=BG, fg='black')
		label_title.pack(pady=PAD, padx=PAD)
		
		# Text Zone
		start_time_label = Label(self.cti_info_frame_L, text="Start time   :", bg=BG, font=("Courrier", 22))
		stop_time_label = Label(self.cti_info_frame_L, text="Stop time    :", bg=BG, font=("Courrier", 22))
		default_label = Label(self.cti_info_frame_L, text="Default value:", bg=BG, font=("Courrier", 22))
		start_time_label.pack(fill=X, pady=PAD, padx=PAD, anchor="nw")
		stop_time_label.pack(fill=X, pady=PAD, padx=PAD, anchor="nw")
		default_label.pack(fill=X, pady=PAD, padx=PAD, anchor="nw")
		
		# Text Zone
		self.start_time_entry.insert(0, str(self.sig_min_time_s))
		self.stop_time_entry.insert(0, str(self.sig_max_time_s))
		self.default_entry.insert(0, str(0))
		self.start_time_entry.pack(fill=X, pady=PAD * 1.1, padx=PAD)
		self.stop_time_entry.pack(fill=X, pady=PAD * 1.1, padx=PAD)
		self.default_entry.pack(fill=X, pady=PAD * 1.1, padx=PAD)
		
		# Buttons
		cti_button = Button(self.cti_button_frame, text="Exit", font=("Courrier", 22), bg=BG_BUTTON,
		                    command=self.cti_exit_fct)
		cti_button.pack(fill=X, pady=PAD, padx=PAD)
		cti_button = Button(self.cti_button_frame, text="Continue", font=("Courrier", 22), bg=BG_BUTTON,
		                    command=self.cti_continue)
		cti_button.pack(fill=X, pady=PAD, padx=PAD)
		
		# Protocol
		self.cti.protocol("WM_DELETE_WINDOW", self.cti_continue)
		
		# Pack
		self.cti_title_frame.pack(side=TOP, fill=X, pady=PAD, padx=PAD)
		self.cti_button_frame.pack(side=BOTTOM, fill=X, pady=PAD, padx=PAD)
		self.cti_info_frame_L.pack(side=LEFT, fill=BOTH, pady=PAD, padx=PAD)
		self.cti_info_frame_R.pack(side=RIGHT, expand=YES, fill=BOTH, pady=PAD, padx=PAD)
		
		# Run Welcome page
		self.cti.mainloop()
	
	def cti_continue(self):
		try:
			start = float(self.start_time_entry.get())
			stop = float(self.stop_time_entry.get())
			default = float(self.default_entry.get())
			if (start >= 0) and (stop >= 0) and (default >= 0) and (stop > start):
				print("t_start     = " + str(start))
				print("t_stop      = " + str(stop))
				print("default_val = " + str(default))
				self.chg_time_int_fct(start, stop, default)
			else:
				ctypes.windll.user32.MessageBoxW(0, "information must not be negative...", "Negative information...", 0)
			self.cti.quit()
		except ValueError:
			ctypes.windll.user32.MessageBoxW(0, "There is no float or int value...", "Not correct information...", 0)
			self.cti.quit()
	
	def cti_exit_fct(self):
		self.stop_fct(TRUE)
		self.cti.quit()


class DecimateSignal:
	def __init__(self, def_dec_val):
		self.dec_val = int(def_dec_val)
		self.ds = Tk()
		self.ds.title("Comtrade Manager Decimation")
		self.ds.geometry("960x480")
		self.ds.minsize(720, 320)
		self.ds.iconbitmap("heia_logo3.ico")
		self.ds.config(background=BG)
		# init comp
		self.ds_title_frame = Frame(self.ds, bg=BG)
		self.ds_info_frame_L = Frame(self.ds, bg=BG)
		self.ds_info_frame_R = Frame(self.ds, bg=BG)
		self.ds_button_frame = Frame(self.ds, bg=BG)
		# Text Zone
		self.factor_entry = Entry(self.ds_info_frame_R, fg="black", bg="#d4d4d4", font=("Courrier", 22))
	
	def ds_launch(self) -> int:
		# Title
		label_title = Label(self.ds_title_frame, text="Select the decimation factor",
		                    font=("Courrier", 25), bg=BG, fg='black')
		label_title.pack(pady=PAD, padx=PAD)
		
		# Text Zone
		offset_label = Label(self.ds_info_frame_L, text="Decimation factor:", bg=BG, font=("Courrier", 22))
		offset_label.pack(fill=X, pady=PAD, padx=PAD, anchor="nw")
		
		# Text Zone
		self.factor_entry.insert(0, str(self.dec_val))
		self.factor_entry.pack(fill=X, pady=PAD * 1.1, padx=PAD)
		
		# Buttons
		ds_button = Button(self.ds_button_frame, text="Exit", font=("Courrier", 22), bg=BG_BUTTON,
		                   command=self.ds_exit_fct)
		ds_button.pack(fill=X, pady=PAD, padx=PAD)
		ds_button = Button(self.ds_button_frame, text="Continue", font=("Courrier", 22), bg=BG_BUTTON,
		                   command=self.ds_continue)
		ds_button.pack(fill=X, pady=PAD, padx=PAD)
		
		# Protocol
		self.ds.protocol("WM_DELETE_WINDOW", self.ds_exit_fct)
		
		# Pack
		self.ds_title_frame.pack(side=TOP, fill=X, pady=PAD, padx=PAD)
		self.ds_button_frame.pack(side=BOTTOM, fill=X, pady=PAD, padx=PAD)
		self.ds_info_frame_L.pack(side=LEFT, fill=BOTH, pady=PAD, padx=PAD)
		self.ds_info_frame_R.pack(side=RIGHT, expand=YES, fill=BOTH, pady=PAD, padx=PAD)
		
		# Run Welcome page
		self.ds.mainloop()
		return self.dec_val
	
	def ds_continue(self):
		try:
			self.dec_val = int(self.factor_entry.get())
			print("dec_val     = " + str(self.dec_val))
			self.ds.quit()
		except ValueError:
			ctypes.windll.user32.MessageBoxW(0, "There is no float or int value...", "Not correct information...", 0)
			self.ds.quit()
	
	def ds_exit_fct(self):
		self.dec_val = 1
		self.ds.quit()


class TimeOffset:
	def __init__(self, signal: Signal, stop_fct, time_offset):
		self.stop_fct = stop_fct
		self.time_offset_fct = time_offset
		self.sig_name = signal.name
		self.sig_min_time_s = float(signal.get_min_time())
		print("min_val      = " + str(-self.sig_min_time_s))
		self.to = Tk()
		self.to.title("Comtrade Manager Change Time Offset")
		self.to.geometry("960x480")
		self.to.minsize(720, 320)
		self.to.iconbitmap("heia_logo3.ico")
		self.to.config(background=BG)
		# init comp
		self.to_title_frame = Frame(self.to, bg=BG)
		self.to_info_frame_L = Frame(self.to, bg=BG)
		self.to_info_frame_R = Frame(self.to, bg=BG)
		self.to_button_frame = Frame(self.to, bg=BG)
		# Text Zone
		self.offset_entry = Entry(self.to_info_frame_R, fg="black", bg="#d4d4d4", font=("Courrier", 22))
	
	def to_launch(self):
		# Title
		label_title = Label(self.to_title_frame, text=("Add Time interval of : " + self.sig_name),
		                    font=("Courrier", 25), bg=BG, fg='black')
		label_title.pack(pady=PAD, padx=PAD)
		
		# Text Zone
		offset_label = Label(self.to_info_frame_L, text="Offset time   :", bg=BG, font=("Courrier", 22))
		offset_label.pack(fill=X, pady=PAD, padx=PAD, anchor="nw")
		
		# Text Zone
		if self.sig_min_time_s != 0:
			self.offset_entry.insert(0, str(-self.sig_min_time_s))
		else:
			self.offset_entry.insert(0, str(0))
		self.offset_entry.pack(fill=X, pady=PAD * 1.1, padx=PAD)
		
		# Buttons
		to_button = Button(self.to_button_frame, text="Exit", font=("Courrier", 22), bg=BG_BUTTON,
		                   command=self.to_exit_fct)
		to_button.pack(fill=X, pady=PAD, padx=PAD)
		to_button = Button(self.to_button_frame, text="Continue", font=("Courrier", 22), bg=BG_BUTTON,
		                   command=self.to_continue)
		to_button.pack(fill=X, pady=PAD, padx=PAD)
		
		# Protocol
		self.to.protocol("WM_DELETE_WINDOW", self.to_continue)
		
		# Pack
		self.to_title_frame.pack(side=TOP, fill=X, pady=PAD, padx=PAD)
		self.to_button_frame.pack(side=BOTTOM, fill=X, pady=PAD, padx=PAD)
		self.to_info_frame_L.pack(side=LEFT, fill=BOTH, pady=PAD, padx=PAD)
		self.to_info_frame_R.pack(side=RIGHT, expand=YES, fill=BOTH, pady=PAD, padx=PAD)
		
		# Run Welcome page
		self.to.mainloop()
	
	def to_continue(self):
		try:
			offset = float(self.offset_entry.get())
			print("t_offset     = " + str(offset))
			if offset >= -self.sig_min_time_s:
				# print("t_offset     = " + str(offset))
				self.time_offset_fct(offset)
			else:
				ctypes.windll.user32.MessageBoxW(0, "final time must not be negative...", "Negative result...", 0)
			self.to.quit()
		except ValueError:
			ctypes.windll.user32.MessageBoxW(0, "There is no float or int value...", "Not correct information...", 0)
			self.to.quit()
	
	def to_exit_fct(self):
		self.stop_fct(TRUE)
		self.to.quit()


if __name__ == '__main__':
	Gui = LogicCmtManager()
