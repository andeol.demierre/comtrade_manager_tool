# -*- coding: utf-8 -*-
#####################################################################
# Author  : Lucas Andrey
# Date    : 17.10.2021
# Version : 1.1
# Mail    : lucas.andrey@hefr.ch
#####################################################################
# MIT License
#
# Copyright (c) 2021 Lucas Andrey
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import numpy as np
import math
import matplotlib.pyplot as plt
import datetime as dt
# import scipy.signal as scipy_sig


NB_PERIOD = 1


# return a linear n-points array from a(included) to b(depend to endpoint)
def my_lin_space(a: float, b: float, n: int):  # , endpoint="included"):
	if (a < b) and (1000000 >= n > 0):
		# if endpoint == "not included":
		# 	new_vector = np.zeros(n)
		# 	space = (b - a) / n
		# 	for i in range(n):
		# 		new_vector[i] = a + space * i
		# elif endpoint == "included":
		# 	new_vector = np.zeros(n)
		# 	space = (b - a) / (n - 1)
		# 	for i in range(n):
		# 		new_vector[i] = a + (space * i)
		# else:
		# 	new_vector = 0.0
		temp = np.linspace(a, b, num=n, endpoint=True, dtype=float)
		new_vector = np.around(temp, decimals=5)
		return new_vector
	else:
		print("ERROR: b must be greater than a")
		print("       n must be positive and less than 1'000'001")
		return 0.0


def plot_show(plot):
	plot.show()


class SignalInfo:
	def __init__(self, name: str, unit: str, start_dt: dt.datetime, trig_dt: dt.datetime):
		self.name = str(name)
		self.unit = str(unit)
		self.start_dt = start_dt
		self.trig_dt = trig_dt


class Signal:
	def __init__(self, name: str, unit: str, init_size: int):
		self._name = str(name)
		self._unit = str(unit)
		self._size = int(init_size)
		self._start_dt = dt.datetime.now(dt.timezone.utc)
		self._trig_dt = dt.datetime.now(dt.timezone.utc)
		self._time = np.zeros(init_size)
		self._data = np.zeros(init_size)
		for n in range(init_size):
			self._time[n] = n
		# print("signal " + self._name + " has been created")

	# def __del__(self):
	# 	print("signal " + self._name + " has been deleted")

	def change_size_0_order(self, new_size: int):  # 0-order interpolation
		if new_size >= 10:
			new_data = np.zeros(new_size)
			new_time = my_lin_space(self.get_min_time(), self.get_max_time(), new_size)
			for new in range(new_size):
				if (new % 1000) == 0:
					print(new)
				old = 0
				end_loop = 0
				while (end_loop == 0) and (old < self._size):
					if new_time[new] >= self._time[old]:
						new_data[new] = self._data[old]
					elif new_time[new] < self._time[old]:
						end_loop = 1
					old += 1
			self._data = np.zeros(new_size)
			self._time = np.zeros(new_size)
			self._size = new_size
			self._data = new_data
			self._time = new_time
			print("resize is successful")
		else:
			print("ERROR: size must be greater than 10...")

	def decimate(self, n: int, sn=int(0)):
		if (2 <= n <= 10) and (0 <= sn < n):
			approx_size = math.ceil(self._size / n)
			if self._size < (approx_size * n):
				new_size = approx_size - 1
			else:
				new_size = approx_size
			print(new_size)
			new_data = np.zeros(new_size)
			new_time = np.zeros(new_size)
			for i in range(new_size):
				index = i * n + sn
				# if i % 1000 == 0:
				# 	print(index)
				new_data[i] = self._data[index]
				new_time[i] = self._time[index]
			self._size = new_data.size
			self._data = new_data
			self._time = new_time
		else:
			print("ERROR: n must be greater than 1 and less than 11 and sn must be less than n...")

	def match_fs_0_order(self, new_fs: float):  # 0-order interpolation
		new_size = int(round((self.get_max_time() - self.get_min_time()) * new_fs + 1, 0))
		if new_size >= 10:
			new_data = np.zeros(new_size)
			new_time = my_lin_space(self.get_min_time(), self.get_max_time(), new_size)
			for new in range(new_size):
				old = 0
				end_loop = 0
				while (end_loop == 0) and (old < self._size):
					if new_time[new] >= self._time[old]:
						new_data[new] = self._data[old]
					elif new_time[new] < self._time[old]:
						end_loop = 1
					old += 1
			self._data = np.zeros(new_size)
			self._time = np.zeros(new_size)
			self._size = new_size
			self._data = new_data
			self._time = new_time
			print("resize is successful")
		else:
			print("ERROR: size must be greater than 10...")

	@property
	def name(self) -> str:
		return self._name

	def change_name(self, name: str):
		self._name = name

	@property
	def unit(self) -> str:
		return self._unit

	def load_unit(self, unit: str):
		self._unit = unit

	@property
	def start_dt(self) -> dt.datetime:
		return self._start_dt

	def load_start_dt(self, new_start_dt: dt.datetime):
		self._start_dt = new_start_dt

	@property
	def trig_dt(self) -> dt.datetime:
		return self._trig_dt

	def load_trig_dt(self, new_trig_dt: dt.datetime):
		self._trig_dt = new_trig_dt

	@property
	def data(self):
		return self._data

	def load_data(self, mes_val: np):
		if self._size != len(mes_val):
			print("Data length isn't correct...")
		else:
			self._data = np.zeros((len(mes_val)))
			self._data = mes_val
			# print("Data loaded")

	def linear_trans_data(self, a: float, b: float):  # It modifies the value of each of the values of the signal
		new_data = np.zeros(self._size)  # by the result of the following operation: y = a * x + b)
		for i in range(self._size):
			new_data[i] = ((a * self._data[i]) + b)
		self._data = new_data

	def get_data(self, n: int):  # returns the (n)th value of the signal (1=>size)
		if (n > 0) and (n <= self._size):
			return self._data[n - 1]
		else:
			print("n must have a value ranging from 1 to the signal size")
			return 0

	def get_max_data(self):
		return np.max(self._data)

	def get_min_data(self):
		return np.min(self._data)

	def get_mean_data(self):
		return np.mean(self._data)

	def remove_last_data(self, n: int):
		if (self._size - 10) > n > 0:
			new_size = self._size - n
			new_data = np.zeros(new_size)
			new_time = np.zeros(new_size)
			for i in range(new_size):
				new_data[i] = self._data[i]
				new_time[i] = self._time[i]
			self._size = new_size
			self._data = new_data
			self._time = new_time
			# print("data deletion is successful")
		else:
			print("ERROR: n must be positive and less than (actual_size - 10)")

	def remove_first_data(self, n: int):
		if (self._size - 10) > n > 0:
			new_size = self._size - n
			new_data = np.zeros(new_size)
			new_time = np.zeros(new_size)
			for i in range(new_size):
				new_data[i] = self._data[i + n]
				new_time[i] = self._time[i + n]
			self._size = new_size
			self._data = new_data
			self._time = new_time
			# print("data deletion is successful")
		else:
			print("ERROR: n must be positive and less than (actual_size - 10)")

	@property
	def time(self):
		return self._time

	def get_time(self, n: int):  # returns the (n)th value of the signal (1=>size)
		if (n > 0) and (n <= self._size):
			return self._time[n - 1]
		else:
			print("n must have a value ranging from 1 to the signal size")
			return 0

	def load_time(self, time_val_s):
		if self._size == len(time_val_s):
			self._time = np.zeros((len(time_val_s)))
			self._time = time_val_s
			# print("Data loaded")
		else:
			print("Time and data are not same length...")

	def get_max_time(self):
		#return np.max(self._time)
		return self._time[-1]

	def get_min_time(self):
		# return np.min(self._time)
		return self._time[0]

	def offset_time(self, offset: float):
		new_time = np.zeros(self._size)
		for i in range(self._size):
			new_time[i] = round(offset + self._time[i], 6)
		self._time = new_time

	def change_time_interval(self, time_min: float, time_max: float, default_dat: float):
		new_size = int(round((time_max - time_min) * self.get_sample_freq() + 1, 0))
		# print(new_size)
		if new_size >= 10:
			new_data = np.zeros(new_size)
			new_time = my_lin_space(time_min, time_max, new_size)
			base_old = 0
			for new in range(new_size):
				old = base_old
				end_loop = 0
				if (new % 1000) == 0:
					print(new)
				while (end_loop == 0) and (old < self._size):
					if new_time[new] > self.get_max_time():
						new_data[new] = default_dat
						end_loop = 1
						base_old = old
					# elif new_time[new] >= self._time[old]:
					# 	new_data[new] = self._data[old]
					elif new_time[new] < self.get_min_time():
						new_data[new] = default_dat
						end_loop = 1
						base_old = old
					elif new_time[new] < self._time[old]:
						new_data[new] = self._data[old-1]
						end_loop = 1
						base_old = old
					old += 1
					if old % 1000 == 0:
						print(old)
				# if new % 1000 == 0:
				# 	print(new)
			self._data = np.zeros(new_size)
			self._time = np.zeros(new_size)
			self._size = new_size
			self._data = new_data
			self._time = new_time
			print("resize is successful")
		else:
			print("ERROR: size must be greater than 10...")

	@property
	def size(self):
		return self._size

	def get_sample_freq(self):
		calculate_sample_rates = round(1 / (self._time[1] - self._time[0]), 4)
		return calculate_sample_rates

	def plot(self):
		plt.figure(self.name)
		plt.plot(self._time, self._data, '-')
		plt.xlabel("Time (s)")
		plt.ylabel("Values (" + self._unit + ")")
		plt.legend([self.name])
		plt.grid(True)
		plt.show()

	def multi_plot(self, plot, fig_name: str):
		plot.figure(fig_name)
		plot.plot(self._time, self._data, '-')
		plot.xlabel("Time (s)")
		plot.ylabel("Values (" + self._unit + ")")
		plot.grid(True)
		return plot


class MultiSignal:
	def __init__(self):
		self._nb_sig = int(0)
		self.sig = list()

	def add_new_sig(self, new_sig: Signal):
		self.sig.append(new_sig)
		self._nb_sig = self._nb_sig + 1

	def pop_sig(self, n: int):
		if self._nb_sig > n > -1:
			temp = self.sig.pop(n)
			del temp
			self._nb_sig = self._nb_sig - 1
			print(n)
			print(self._nb_sig)

	def ret_signal(self, n: int) -> Signal:
		if self._nb_sig > n > -1:
			return self.sig[n]

	def ret_signal_name(self, n: int) -> str:
		if self._nb_sig > n > -1:
			return self.sig[n].name

	def nb_sig(self):
		return self._nb_sig

	def ret_name_list(self) -> list:
		list_name = []
		for i in range(self._nb_sig):
			list_name.append(str(self.sig[i].name))
			# print(str(self.sig[i].name))
		return list_name

	def plot_sel_sig(self, sig_list: list):
		label = list(str())
		for n in sig_list:
			if isinstance(n, int) and (self._nb_sig > n > -1):
				label.append(self.sig[n].name + " : " + self.sig[n].unit)
				self.sig[n].multi_plot(plt, "Selected Signals")
		plt.legend(label)
		plt.show()

	def print_sel_sig_info(self, sig_list: list):
		print(" ")
		for n in sig_list:
			if isinstance(n, int) and (self._nb_sig > n > -1):
				print("*---------------------------------------------------------*")
				print("Signal " + self.sig[n].name + " informations :")
				print(" - Index     : " + str(n))
				print(" - Unit      : " + self.sig[n].unit)
				print(" - Length    : " + str(self.sig[n].size))
				print(" - T_start   : " + self.sig[n].start_dt.strftime("%d/%m/%Y,%H:%M:%S.%f"))
				print(" - T_trigger : " + self.sig[n].trig_dt.strftime("%d/%m/%Y,%H:%M:%S.%f"))
				print("*---------------------------------------------------------*")
				print(" ")

	def compute_sel_sig_freq(self, sig_list: list):
		if sig_list:
			nb_data = len(sig_list)
			nb_valid_sig = 0
		else:
			print("Zero signal are selected")
			return 0
		size_of_data = self.sig[sig_list[0]].size
		for n in sig_list:
			if size_of_data == self.sig[n].size:
				nb_valid_sig += 1
			else:
				print("size is different after " + str(nb_valid_sig) + "signals")
				return

		t_min = 0.0083
		phases_sig = list()
		legend = list()
		estimate_freq = 50
		calculate_sample_rates = (size_of_data - 1) / (self.sig[sig_list[0]].time[size_of_data - 1] -
													self.sig[sig_list[0]].time[0])
		if calculate_sample_rates < 1500:
			nb_period = NB_PERIOD * 3
			nb_cross = 2 * nb_period + 1
		elif calculate_sample_rates < 5000:
			nb_period = NB_PERIOD * 2
			nb_cross = 2 * nb_period + 1
		else:
			nb_period = NB_PERIOD
			nb_cross = 2 * nb_period + 1

		# ref_freq = np.zeros(size_of_data)
		plot_freq = np.zeros(size_of_data)
		ref_time = np.zeros(size_of_data)
		for i in range(size_of_data):
			ref_time[i] = self.sig[sig_list[0]].time[i]   # 0.001 + i / calculate_sample_rates

		for n in sig_list:
			if isinstance(n, int) and (self._nb_sig > n > -1) and (self.sig[n].size == size_of_data):
				phases_sig.append(self.sig[n].data)
				legend.append(self.sig[n].name + " : " + self.sig[n].unit)
			else:
				print("Error with selected signals list")
				return

		# --FFT---------------------------------------------------------------------------------------------------------
		plt.figure("TF")
		freq = calculate_sample_rates * np.fft.fftfreq(size_of_data)
		for i in range(nb_data):
			fft = np.fft.fft(phases_sig[i]) / size_of_data
			norm = (fft.real * fft.real) + (fft.imag * fft.imag)
			plt.plot(freq, norm)
			plt.xlabel("Frequency (Hz)")
			plt.ylabel("Time (s)")
		plt.legend(legend)
		plt.grid(True)

		# --Inst Freq---------------------------------------------------------------------------------------------------
		print("Estimation de la fréquence instantannée...")
		# Calcul de la fréquence instantannée des signaux
		plt.figure("FREQ_INST")
		freq_inst = np.zeros((nb_data, size_of_data))
		freq_calc = np.zeros(size_of_data)
		cross_time = np.zeros(nb_cross)
		for i in range(nb_data):
			data = phases_sig[i]
			last_freq = estimate_freq
			for j in range(size_of_data):
				rec_time_j = ref_time[j]
				if j == 0:
					freq_inst[i][j] = last_freq
					cross_time = np.zeros(nb_cross)
				elif (data[j - 1] < 0) and (data[j] >= 0) and (cross_time[0] == 0.0) and (
						(rec_time_j - cross_time[nb_cross - 1]) >= t_min):
					# print("UP",rec_time_j,cross_time)
					freq_inst[i][j] = last_freq
					for k in range(nb_cross - 1):
						cross_time[k] = cross_time[k + 1]
					cross_time[nb_cross - 1] = rec_time_j - (
								abs(data[j]) / (calculate_sample_rates * (abs(data[j]) + abs(data[j - 1]))))
				#
				elif (data[j - 1] > 0) and (data[j] <= 0) and (cross_time[0] == 0.0) and (
						(rec_time_j - cross_time[nb_cross - 1]) >= t_min):
					# print("DOWN",rec_time_j,cross_time)
					freq_inst[i][j] = last_freq
					for k in range(nb_cross - 1):
						cross_time[k] = cross_time[k + 1]
					cross_time[nb_cross - 1] = rec_time_j - (
								abs(data[j]) / (calculate_sample_rates * (abs(data[j]) + abs(data[j - 1]))))
				#
				elif (data[j - 1] < 0) and (data[j] >= 0) and ((rec_time_j - cross_time[nb_cross - 1]) >= t_min):
					for k in range(nb_cross - 1):
						cross_time[k] = cross_time[k + 1]
					cross_time[nb_cross - 1] = rec_time_j - (
								abs(data[j]) / (calculate_sample_rates * (abs(data[j]) + abs(data[j - 1]))))
					freq_inst[i][j] = (nb_cross - 1) / ((cross_time[nb_cross - 1] - cross_time[0]) * 2)
					last_freq = freq_inst[i][j]
				elif (data[j - 1] > 0) and (data[j] <= 0) and ((rec_time_j - cross_time[nb_cross - 1]) >= t_min):
					for k in range(nb_cross - 1):
						cross_time[k] = cross_time[k + 1]
					cross_time[nb_cross - 1] = rec_time_j - (
								abs(data[j]) / (calculate_sample_rates * (abs(data[j]) + abs(data[j - 1]))))
					freq_inst[i][j] = (nb_cross - 1) / ((cross_time[nb_cross - 1] - cross_time[0]) * 2)
					last_freq = freq_inst[i][j]
				else:
					freq_inst[i][j] = last_freq
		# plt.plot(rec.time, freq_inst[i])

		print("Moyenne sur les 3 phases...")
		for j in range(size_of_data):
			for i in range(nb_data):
				freq_calc[j] += freq_inst[i][j] / nb_data

		print("Affichage des signaux...")

		# Affichage des signaux
		# frequency
		plt.plot(ref_time, freq_calc)
		# plt.plot(ref_time, plot_freq)
		plt.xlabel("Time (s)")
		plt.ylabel("Frequency (Hz)")
		plt.legend(('Freq_calc', 'Freq_ref'))
		plt.grid(True)

		# 3 phases
		plt.figure("Tri_Phases_SIG")
		for i in range(nb_data):
			plt.plot(ref_time, phases_sig[i])  # , 'b-o')
		# plt.xlim((1.95,2.05))
		plt.xlabel("Time (s)")
		plt.ylabel("Voltage (V)")
		plt.legend(legend)
		plt.grid(True)

		# --Plot All Signals--------------------------------------------------------------------------------------------
		plt.show()

		# --Add crated signal-------------------------------------------------------------------------------------------
		freq_inst_sig = Signal(self.sig[sig_list[0]].name + "_inst_freq", "Hz", size_of_data)
		freq_inst_sig.load_data(freq_calc)
		freq_inst_sig.load_time(self.sig[sig_list[0]].time)
		self.add_new_sig(freq_inst_sig)


if __name__ == '__main__':
	print("heia_signal_toolbox")
